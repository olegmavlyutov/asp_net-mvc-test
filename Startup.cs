﻿using MvcMovie.Infrastructure;
using MvcMovie.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace MvcMovie
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = AppConstants.SimpleCookieAuthenticationScheme;
            })
                .AddScheme<AuthenticationSchemeOptions, SimpleCookieAuthenticationHandler>(AppConstants.SimpleCookieAuthenticationScheme, null);

            services.AddAuthorization(options =>
            {
                options.AddPolicy(AppConstants.AdminClaimPolicy, policy =>
                {
                    policy.RequireClaim(AppConstants.SimpleCookieAuthenticationSchemeRoleClaimType,
                        AppConstants.SimpleCookieAuthenticationSchemeAdminRole);
                });
            });

            services.AddDbContext<MvcMovieContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("MvcMovieContext")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment()) // if app in development
            {
                app.UseDeveloperExceptionPage(); // throw development error
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            loggerFactory.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "logger.txt"));
            var logger = loggerFactory.CreateLogger("FileLogger");

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting(); // use routing

            app.UseAuthorization();
            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => // add addresses which will be use
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
