﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MvcMovie.Models;
using MvcMovie.Infrastructure;

namespace MvcMovie.Controllers
{
    public class CalculatorController : Controller
    {
        public IActionResult GenerateCookie(string username)
        {
            Response.Cookies.Append(AppConstants.SimpleCookieAuthenticationSchemeCookieName, username);
            return Content("Cookie has been planted.");
        }

        [Authorize(AuthenticationSchemes = AppConstants.SimpleCookieAuthenticationScheme, Policy = AppConstants.AdminClaimPolicy)]

        public ActionResult Index()
        {
            return View(new Calculator());
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = AppConstants.SimpleCookieAuthenticationScheme, Policy = AppConstants.AdminClaimPolicy)]
        public ActionResult Index(Calculator c, string operations)
        {
            if (operations == "+")
            {
                c.Result = c.FirstNumber + c.SecondNumber;
            }
            else if (operations == "-")
            {
                c.Result = c.FirstNumber - c.SecondNumber;
            }
            else if (operations == "*")
            {
                c.Result = c.FirstNumber * c.SecondNumber;
            }
            else
            {
                c.Result = c.FirstNumber / c.SecondNumber;
            }
            return View(c);
        }
    }
}
